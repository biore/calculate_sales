package jp.alhinc.nosaka_akinori.calculate_sales;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {


	public static void main(String[] args) {
		try{
			branchSale(args);
		}catch(Exception a) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}


	public static void branchSale(String[] args) throws Exception {
		Path inputFilePath = Paths.get(args[0], "branch.lst");

		// branch.lst の読み込み 支店コード
		Map<String, Branch> branchMap = subsucBranch(inputFilePath);
		if(branchMap == null) {
			return;
		}

		// 該当ファイルのみ抽出
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return new File(dir, filename).isFile() && filename.matches("^[0-9]{8}\\.rcd$");
			}
		};

		// ファイルの番号だけを数列にしてsort
		File[] rcdfiles = new File(args[0]).listFiles(filter);
		Arrays.sort(rcdfiles);

		int minimum = Integer.parseInt(rcdfiles[0].getName().substring(0, 8));
		int maximum = Integer.parseInt(rcdfiles[rcdfiles.length - 1].getName().substring(0, 8));

		if (minimum + rcdfiles.length - 1 != maximum) {
			System.out.println("売上ファイル名が連番になっていません");
			return;
		}

		// 売り上げファイルの中身を確認-集計
		for (File rcdfile: rcdfiles) {
			Path filePath = Paths.get(rcdfile.getPath());
			List<String> salesLine = Files.readAllLines(filePath, StandardCharsets.UTF_8);

			String code = salesLine.get(0);
			String sale = salesLine.get(1);

			if (!(code.matches("^[0-9]{3}$") && sale.matches("^[0-9]+$") && salesLine.size() == 2)){
				System.out.println(rcdfile.getName() + "のフォーマットが不正です");
				return;
			}

			if (!branchMap.containsKey(code)) {
				System.out.println(rcdfile.getName() + "の支店コードが不正です");
				return;
			}

			Branch branch = branchMap.get(code);
			if (branch.isNotValidDigit()) {
				System.out.println("合計金額が10桁を超えました");
				return;
			}
			branchMap.get(code).addSale(Long.parseLong(sale));
		}

		Path outputFilePath = Paths.get(args[0], "branch.out");
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(outputFilePath.toString()));
			for (Branch sale: branchMap.values()) {
				bw.append(sale.toString());
				bw.newLine();
			}
		}finally {
			if (bw != null) bw.close();
		}
	}


//   支店コード登録関数
	public static Map<String, Branch> subsucBranch(Path file) throws IOException {
		Map<String, Branch> branchMap = new HashMap<>();

		if (Files.notExists(file)) {
			System.out.println("支店定義ファイルが存在しません");
			return null;
		}

		List <String> lines = Files.readAllLines(file, StandardCharsets.UTF_8);

		for (String line : lines) {
			String[] lists = line.split(",");
			if (!line.matches("^[0-9]{3},[^ ,]+$")) {
				System.out.println("支店定義ファイルのフォーマットが不正です");
				return null;
			}

			String code = lists[0];
			String name = lists[1];
			branchMap.put(code, new Branch(code, name));
		}
		return branchMap;
	}
}


class Branch{
	private String code;
	private String name;
	private Long sales;

	Branch(String code, String name){
		this.name = name;
		this.code = code;
		this.sales = 0L;
	}

	public void addSale(Long sale) {
		this.sales += sale;
	}

	public Long getSumSales() {
		return this.sales;
	}

	@Override
	public String toString() {
		return this.code + "," + this.name + "," + this.sales;
	}

	public boolean isNotValidDigit() {
		return this.sales.toString().length() > 10;
	}
}